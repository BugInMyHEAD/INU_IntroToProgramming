ordA = ord('A')
ordNextZ = ord('Z') + 1
#    ABCDEFGHIJKLMNOPQRSTUVWXYZ
a = 'KGVBFEHJLDUNPQRMTOXCYSZWAI'
b = [ None for i1 in range(26) ]
for i1 in range(26):
	b[ord(a[i1]) - ordA] = chr(ordA + i1)

print('ip060801c.py')
str_from_user = [ input('Enter a string:\n') ]
for ch0_1 in str_from_user[0]:
	ordch0_1 = ord(ch0_1)
	if ordA <= ordch0_1 < ordNextZ:
		print(a[ordch0_1 - ordA], end='')
	else:
		print(ch0_1, end='')
