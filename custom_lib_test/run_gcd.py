import sys
from custom_lib.gcd import *


print('run_gcd.py')

a = int(input('a = '))
b = int(input('b = '))

if a == 0 or b == 0:
	print('You have input 0')
	sys.exit(1)

print(gcd(a, b))
