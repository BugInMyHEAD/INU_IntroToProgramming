from custom_lib.lcm import *
from custom_lib.is_perfect_number import *

print('세 수의 최소공배수')
a = int(input('a = '))
b = int(input('b = '))
c = int(input('c = '))
print(lcm(a, b, c))

print()

print('2부터 N까지 완전수')
n = int(input('n = '))
for i1 in range(2, n + 1):
	if is_perfect_number(i1):
		print(i1, end=' ')
