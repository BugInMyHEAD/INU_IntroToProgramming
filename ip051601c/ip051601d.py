fout = open('output.txt', 'w+')
for i1 in range(0, 51, 10):
	i2 = int(i1 * 9 / 5 + 32)
	fout.write('%d degrees Celsius, %d degrees Fahrenheit\n' % (i1, i2))
fout.seek(0)
print(fout.readline())
fout.close()

fin = open('output.txt')
line = '\0'
while line:
	print(line, end='')
	line = fin.readline()
fin.close()
