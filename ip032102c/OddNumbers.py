rangeFromUser = int(input('N = '))
oddOrEvenFromUser = int(input('홀수/짝수 선택(1:홀수,2:짝수):'))
if oddOrEvenFromUser != 1 and oddOrEvenFromUser != 2:
	exit(16)

for i1 in range(oddOrEvenFromUser, rangeFromUser + 1, 2):
	print(i1, end=' ')

'''''
i0 = oddOrEvenFromUser
while i0 <= rangeFromUser:
	print(i0, end=' ')
	i0 += 2
'''

'''''
for i1 in range(oddOrEvenFromUser, rangeFromUser + 1):
	if i1 % 2 == 1:
		print(i1, end=' ')
'''

'''''
i0 = oddOrEvenFromUser
while i0 <= rangeFromUser:
	if i0 % 2 == 1:
		print(i0, end=' ')
	i0 += 1
'''
