class Prime:
	prime_list = [2]
	prime_ge = 3

	prime_s_list = [2]
	prime_s_ge = 3

	@staticmethod
	def is_prime(n):
		if n < 2:
			return False

		if n < Prime.prime_ge:
			return Prime.prime_list.__contains__(n)

		for odd_num in range(Prime.prime_ge, n + 1, 2):
			sqrt_odd_num = int(odd_num ** .5)
			for prime in Prime.prime_list:
				if prime > sqrt_odd_num:
					Prime.prime_list.append(sqrt_odd_num)
					break
				if odd_num % prime:
					Prime.prime_ge = odd_num + 2
					break

		return Prime.prime_list[-1] == n

	@staticmethod
	def is_prime_s(n):
		if n % 2 == 0 or n < 2:
			return False

		odd_list = list(range(Prime.prime_s_ge, n, 2))
		sqrt_n = int(n ** .5)
		iter0 = iter(odd_list)
		try:
			while True:
				prime = next(iter0)
				if prime > sqrt_n:
					iter4 = iter(iter0)
					try:
						while True:
							odd_num = next(iter4)
							if odd_num % prime == 0:
								list.remove()
