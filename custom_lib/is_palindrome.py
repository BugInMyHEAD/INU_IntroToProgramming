def is_palindrome(s):
	s = ''.join(s.split()).upper()

	i0 = 0
	j0 = len(s) - 1
	while True:
		if i0 > j0:
			result = True
			break
		if s[i0] != s[j0]:
			result = False
			break
		i0 += 1
		j0 -= 1

	return result
