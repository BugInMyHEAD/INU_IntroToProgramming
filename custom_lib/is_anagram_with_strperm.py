def is_anagram_with_strperm(s1, s2):
	s1 = ''.join(s1.split()).upper()
	s2 = ''.join(s2.split()).upper()

	if len(s1) != len(s2):
		return False

	l = strperm(s1)
	for s3 in l:
		if s3 == s2:
			result = True
			break
	else:
		result = False

	return result
