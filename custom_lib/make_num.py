def make_num(s):
	if not s.isnumeric():
		return 0

	result = 0
	for ch in s:
		result = 10 * result + ord(ch) - ord('0')

	return result
