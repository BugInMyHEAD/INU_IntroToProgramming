from custom_lib.gcd import *


def lcm(*nums):
	b = nums[0]
	if b == 0:
		return 0

	for a in nums[1:]:
		if a == 0:
			return 0

		b = a // gcd(a, b) * b

	return b
