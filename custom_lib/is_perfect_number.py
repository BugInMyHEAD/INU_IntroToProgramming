from custom_lib.divisor import *


def is_perfect_number(n):
	s = 0
	for i1 in divisor(n)[0:-1]:
		s += i1

	return s == n
