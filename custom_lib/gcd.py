def gcd(*nums):
	b = nums[0]
	if b == 0:
		return 0

	for a in nums[1:]:
		if a == 0:
			return 0

		# Euclidean algorithm
		while True:
			c = a % b
			if c == 0:
				break
			a = b
			b = c

	return b
