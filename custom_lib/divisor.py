import sys


def divisor(n):
	if n <= 0:
		sys.exit(1)

	little_num_list = [1]
	big_num_list = [n]

	sqrt_of_a = round(n ** .5)
	for i1 in range(2, sqrt_of_a + 1):
		if n % i1 == 0:
			little_num_list.append(i1)
			big_num_list.insert(0, n // i1)

	if little_num_list[-1] > big_num_list[0]:
		little_num_list.pop()
	elif little_num_list[-1] == big_num_list[0]:
		big_num_list.pop(0)

	return little_num_list + big_num_list
