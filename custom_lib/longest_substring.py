def longest_substring(s: str):
	li = []
	maxlen = 0
	nowlen = 0
	i0 = 0
	for i1 in range(len(s) + 1):
		if s[i0:i1].__contains__(s[i1:i1+1]) and nowlen >= maxlen:
			if nowlen > maxlen:
				li.clear()
				maxlen = nowlen
			li.append(s[i0:i1])
			nowlen = 0
			i0 = i1
		nowlen += 1

	return li
