def is_anagram(s1, s2):
	s1 = ''.join(s1.split()).upper()
	s2 = ''.join(s2.split()).upper()

	if len(s1) != len(s2):
		return False

	d = {}

	for ch1 in s1:
		try:
			d[ch1] = d[ch1] + 1
		except KeyError:
			d[ch1] = 1
	for ch1 in s2:
		try:
			d[ch1] = d[ch1] - 1
		except KeyError:
			return False

	for i1 in d.values():
		if i1:
			return False

	return True
