def string_search(s: str, p: str):
	li = []
	it = iter(p)
	i0 = 0
	for i1 in range(len(s) + 1):
		try:
			if next(it) != s[i1]:
				it = iter(p)
				i0 = i1 + 1
		except StopIteration:
			it = iter(p)
			li.append(i0)
		except IndexError:
			break

	return li
