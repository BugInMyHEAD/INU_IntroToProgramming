N = int(input('N = '))
i0 = 1
it = iter(range(1, N + 1))
try:
	i3 = next(it)
	while True:
		for i5 in range(i0):
			print(i3, end=' ')
			i3 = next(it)
		print()
		i0 *= 2
except StopIteration:
	pass
print()
