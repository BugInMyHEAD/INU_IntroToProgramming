def encipher(p, k):
	if k < 1 or 26 < k:
		raise ValueError

	li = []
	for c in p:
		a = ord(c)
		if a == 32:  # space
			a = 96
		t = a + k
		if t > 122:
			t %= 122
		if t < 27:
			t += 95
		if t == 96:
			t = 32
		li.append(chr(t))
	return ''.join(li)


def decipher(p, k):
	if k < 1 or 26 < k:
		raise ValueError

	s = ''
	for c in p:
		a = ord(c)
		if a == 32:  # space
			a = 96
		t = a - k
		if t < 96:
			t += 27
		if t == 96:
			t = 32
		s += chr(t)
	return s


key = 2
x = encipher(input('char= '), key)
print(x)
print(decipher(x, key))
